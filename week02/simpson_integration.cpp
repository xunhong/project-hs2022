#include <iostream>
#include <string>
#include <cmath>
#include "simpson_functions.hpp"
using namespace std;
#define PI  3.14159265358979323846264338327950288


double simpsonInt(double (*fun_p)(double), double a, double b, double num_bins){
    double sum =0;
    double delta = (b-a)/num_bins;
    double arg;

    for (int i = 0; i < num_bins; i++)
    {
         arg = a + i*delta;
        sum += fun_p(arg)  + fun_p(arg + delta) + fun_p(arg + delta/2) * 4;
    }
    return sum * delta / 6;
}

int main(){
    int resolution;
    cin >> resolution;
    cout << "\nWith resolution "<< resolution << ". The result is: "<<simpsonInt(&myFunction1, 0., PI, resolution );
}