#include <iostream>
#include <string>
#include <cmath>
using namespace std;

void printArray(int N, int* array){
    /* print out the array of length N */
    cout << "The array is: ";
    for (int i=0; i<N; i++){
        cout << *(array+i) << " ";
    }
}

int main(){
    int n_input, temp; 
    cout << "Size: ";
    cin >> n_input;
    int* array = new int[n_input];
    for (int i=0; i < n_input; i++){
        cout << "\nThe " << (i+1) << " input: ";
        cin >> temp;
        array[i] = temp;
    }
    cout << "\n" ;
    printArray(n_input, array);
    delete[] array;
    return 0;
}
