#include <iostream>
#include <string>
#include <cmath>
#include <iomanip>
#include "simpson_functions.hpp"
using namespace std;
#define PI  3.14159265358979323846264338327950288


double simpsonInt(double (*fun_p)(double), double a, double b, double num_bins){
    double sum =0;
    double delta = (b-a)/num_bins;
    double arg;

    for (int i = 0; i < num_bins; i++)
    {
         arg = a + i*delta;
        sum += fun_p(arg)  + fun_p(arg + delta) + fun_p(arg + delta/2) * 4;
    }
    return sum * delta / 6;
}

int main(){
    double resolution_set[10] = {2,3,5,10,15,25,40,60,80,100};
    double resolution;
    for (int i = 0; i < 10; i++)
    {
        resolution = resolution_set[i];
        cout << "\n"<< resolution << "\t"<<simpsonInt(&myFunction1, 0., PI, resolution ) << setprecision(10);
    }
}