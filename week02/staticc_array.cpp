#include <iostream>
#include <string>
#include <cmath>
using namespace std;

void print_array(int N, int* array){
    // print out the array of length N
    cout << "\nThe array is: ";
    for (int i=0; i<N; i++){
        cout << array[i] << " ";
    }
}

int main(){
    int n_max = 10;
    int n_input = 11;
    while (n_input > 10){
        cout << "Size should be <= 10. Input: ";
        cin >> n_input;
    }
    int array[10];

    for (int i =0; i< n_input; i++){
        cout << "\nYour " << i+1 << " input: ";
        cin >> array[i];
    }
    print_array(n_input, array);
}