#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class Vector{
	// scope specifier
	public:
		int x,y;

		// default constructor;
		Vector(){
            x=5; y=5; id_= counter_++;
            cout << "constructing " << id_ << endl;
            } 

		// overloaded constructor. NO ";" NEEDED ! 
		Vector(int a, int b) : x(a), y(b), id_(counter_++) {
            cout << "constructing " << id_ << endl;
        }  

		// copy constructor.
		Vector(const Vector& rhs) : id_(counter_++), x(rhs.x), y(rhs.y) {
            cout << "copy constructing" << id_ << endl;
        }

		// assigning/overloaded "=" operator
		Vector& operator=(const Vector& rhs){
			if (this != &rhs){
				x = rhs.x;
				y = rhs.y;
			}
            cout << "assigning " << rhs.id_ << " to " << id_ << endl;
			return *this;
		}

		// overloaded '+' operator
		Vector operator+ (const Vector& vector){
			Vector temp;
			temp.x = vector.x;
			temp.y = vector.y;
			return temp;
		} 

		void print() const {
			cout << "id " << id_ << ": (" << x << "," << y << ")" << endl ;
		}
	private:
		int id_;
        static int counter_;
        
};		

int Vector::counter_ = 0; // static counter has to be initialized outside

int main(){
    Vector a{};
    Vector b{1,2};
    Vector c(a);
    Vector d; 
    d = a;
    Vector& e = a;
    Vector& f = (e.operator=(a));
	Vector g = a;

    a.print();
    b.print();
    c.print();
    d.print();
    e.print();
    f.print();
	g.print();
}