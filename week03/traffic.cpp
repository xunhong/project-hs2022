#include <iostream>
#include <string>
#include <cmath>
using namespace std;

class TrafficLight {
    public:
        enum light {green, orange, red};
        // constructor
        TrafficLight(light l = red) : id_(counter_++), state_(l) {
            cout << "Constructing " << id_ << endl;
        }
        // copy constructor
        TrafficLight(const TrafficLight& rhs): id_(counter_++), state_(rhs.state_) {
            cout << "Copy constructing " << id_ << " from " << rhs.id_ << endl;
        }
        // assigning 
        TrafficLight& operator= (const TrafficLight& rhs){
            cout << "Assigning " << rhs.id_ << " to " << id_ << endl;
            if (this != &rhs) {state_ = rhs.state_;}
            return *this;
        }
        // destructor
        ~TrafficLight(){cout << "Destructing " << id_ << endl;}

        // member function
        void print_state() const;

        static int counter_;
        int id_;
        light state_;
};

int TrafficLight::counter_ = 0;
void TrafficLight::print_state() const {
    cout << id_ <<"  ";
    switch (state_)
    {
    case TrafficLight::red:
        cout << "red" << endl;
        break;
    case TrafficLight::orange:
        cout << "orange" <<endl;
        break;
    case TrafficLight::green:
        cout << "green" << endl;
        break;
    default:
        cout << "broken??" << endl;
        break;
    }
}

int main(){
    TrafficLight a(TrafficLight::green);
    TrafficLight *b_ptr = new TrafficLight(TrafficLight::orange);
    TrafficLight c(a);
    TrafficLight d = *b_ptr;
    TrafficLight e; e.operator=(a);
    //TrafficLight &f = a;
    TrafficLight& g  = (e=a);
    a.print_state();
    b_ptr -> print_state();
    c.print_state();
    d.print_state();
    e.print_state();
    //f.print_state();
    g.print_state();
    cout << "gap\n";
    g.id_++;
    g.print_state();
    e.print_state();
    a.print_state();
    delete b_ptr;
    return 0;
}