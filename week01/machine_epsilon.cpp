#include <iostream>
#include <string>
using namespace std;

int main(){
    float epsilon = 1.0;
    float base = 1.0;
    while ((base + 0.9 * epsilon) != base){
        epsilon *= 0.9;
    }
    cout << epsilon;

}