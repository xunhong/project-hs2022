#include <iostream>
#include <string>
#include <cmath>
using namespace std;

double f_temp(int N, int i);

int main(){
    int N;
    cout << "Type the N: ";
    cin >> N;
    const double pi = M_PI;
    const double deltax = pi / N;
    int i = 0;
    double sum = 0;
    while (i*deltax <= pi - deltax) {
        sum += f_temp(N,i);
        i++;
    }
    cout << deltax/6 * sum;
    return 0;
}

double f_temp(int N, int i){
    const double pi = M_PI;
    const double deltax = pi / N;
    return sin(i*deltax) + 4*sin(i*deltax + deltax/2) + sin((i+1.0)*deltax);
}