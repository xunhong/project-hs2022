/* Programming Techniques for Scientific Simulations, HS 2022
 * Exercise 4.1
 */

#include <complex>
#include <iostream>
using namespace std;

enum Z2 { Plus = 1, Minus = -1};


complex<double> identity_element(complex<double>) { 
  return complex<double>(1.,0);
}
double identity_element(double) { 
  return 1.0;
}
Z2 identity_element(Z2) { 
  Z2 a = Plus;
  return a;
}


Z2 operator*(const Z2 a, const Z2 b)
{
  int temp_a = (a == Plus? 1 : -1);
  int temp_b = (b == Plus? 1 : -1);
  int temp_c = temp_a * temp_b;
  Z2 c = (temp_c == 1 ? Plus : Minus);
  return c;
}

ostream& operator<<(ostream& os, const Z2 a)
{
  if (a == Plus) { os << "Plus";}
  else {os << "Minus";}
  return os;
}

template<class T>
T operator*(const T a, const Z2 b)
{
  T temp_b = (b == Plus ? 1 : -1);
  return temp_b * a;
}

template<class T>
T operator*(const Z2 b, const T a)
{
  T temp_b = (b == Plus ? 1 : -1);
  return temp_b * a;
}

template<class T>
T mypow(T a, unsigned int n)
{
  auto id = identity_element(a);
  if (n == 0) {return id;}
  else {return a * mypow(a, n-1);}
}

int main()
{
  cout << "Plus*Minus = " << Plus*Minus << endl;

  cout << "Plus*-1*Minus = " << Plus*-1*Minus << endl;

  cout << "(1.+3.)*mypow(Minus,4) = " << (1.+3.)*mypow(Minus,4)
            << endl;

  for (unsigned i=0; i<7; ++i)
    cout << "Plus^" << i << " = " << mypow(Plus,i) << endl;

  for (unsigned i=0; i<7; ++i)
    cout << "Minus^" << i << " = " << mypow(Minus,i) << endl;
  for (unsigned i=0; i<7; ++i)
    cout << "2^" << i << " = " << mypow(2.0,i) << endl;

  // For complex numbers
  cout << "Plus*-complex<double>(1., 2.)*Minus = "
            <<  Plus*-complex<double>(1., 2.)*Minus
            << endl;
  cout << "complex<double>(1., 1.)*mypow(Minus,4) = "
            <<  complex<double>(1., 1.)*mypow(Minus,4)
            << endl;
  for (unsigned i=0; i<7; ++i)
    cout << "complex<double>(1., 1.)^" << i << " = "
              << mypow(complex<double>(1., 1.),i)
              << endl;
/**/
  return 0;
}
