/* Programming Techniques for Scientific Simulations, HS 2022
 * Exercise 4.1
 */

#include <complex>
#include <iostream>
using namespace std;

enum Z2 { Plus = 1, Minus = -1};

int main(){
  const Z2 a = Plus;
  const Z2 b = Minus;
  int c = a * b;
  cout << c;
}