#include <iostream>
#include <string>
#include <cmath>
#include <simpson.hpp>
using namespace std;

class MyFunc {
    public:
        double lambda;
        MyFunc(double l = 1.0) : lambda(l) {};
        double operator()(double x){
            return exp(-lambda * x);
        }

};

/*
template<class T>
double simpson(const double a,
                 const double b,
                 const unsigned bins,
                 T function) {

  const unsigned int steps = 2*bins + 1;

  const double dr = (b - a) / (steps - 1);

  double I = function(a);

  for(unsigned int i = 1; i < steps-1; ++i)
    I += 2 * (1.0 + i%2) * function(a + dr * i);

  I += function(b);

  return I * (1./3) * dr;

}
*/

int main() {
MyFunc fun;
cout << simpson(0, 10, 20, fun) << std::endl;
return 0;
}
